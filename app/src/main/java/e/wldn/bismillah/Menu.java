package e.wldn.bismillah;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

//import com.example.ball3.R;


public class Menu extends Activity {
    ImageView tracker;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_layout);
        tracker = (ImageView)findViewById(R.id.imageView1);


        OnClickListener A = new OnClickListener() {

            @Override
            public void onClick(View v) {
                context = getApplicationContext();
                Intent intent = new Intent(context, Devices_list.class);
                startActivity(intent);
            }
        };
        tracker.setOnClickListener(A);
    }
}
